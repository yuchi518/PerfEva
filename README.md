


#### Usage

```sh
./PerfEva 
Usage: ./PerfEva [options] url
  -B string
        Authorization Header, bearer format
  -D int
        delay time, aka. request url after milliseconds, >= 0
  -M    modular mode for random delay time
  -P int
        number of parallel routines, >= 1 (default 1)
  -m string
        5rps: 5 requests/sec; 5rpm: 5 requests/minute; 5rph: 5 requests/hour; legacy: legacy mode, no setting also means legacy mode.
  -n int
        legacy mode: Request n times, >= 1; others: n seconds/minutes/hours (default 1)
```

##### Run mode - Requests per second/minute/hour
Run go file directly
```sh
go run main.go -P 2 -D 2000 -m 100rpm -n 2 \
  -B "aa.bb.cc-dd" \
   "https://localhost/api/v2/users?return_organization_ids=false&return_organizations=false" 
```

Build and run
```sh
go build
./PerfEva -P 2 -D 2000 -n 10 -M \
  -B "aa.bb.cc-dd" \
   "https://localhost/api/v2/users?return_organization_ids=false&return_organizations=false" 
```



##### Run mode - Legacy

Run go file directly
```sh
go run main.go -P 2 -D 2000 -n 10 -M \
  -B "aa.bb.cc-dd" \
   "https://localhost/api/v2/users?return_organization_ids=false&return_organizations=false" 
```

Build and run
```sh
go build
./PerfEva -P 2 -D 2000 -n 10 -M \
  -B "aa.bb.cc-dd" \
   "https://localhost/api/v2/users?return_organization_ids=false&return_organizations=false" 
```


