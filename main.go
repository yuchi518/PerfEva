package main

import (
	"crypto/tls"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

var (
	startTime             time.Time
	numOfParallelRoutines int
	runMode               string
	modularMode           bool
	afterMilliseconds     int
	numOfTimes            int
	bearer                string
	urls                  []string
)

const (
	TimeFormat = "2006-01-02T15:04:05.000"
)

type RequestResult struct {
	startedTime time.Time
	endedTIme   time.Time
	data        []byte
	err         error
}

func init() {
	startTime = time.Now()
	flag.IntVar(&numOfParallelRoutines, "P", 1, "number of parallel routines, >= 1")
	flag.StringVar(&runMode, "m", "", "5rps: 5 requests/sec; 5rpm: 5 requests/minute; 5rph: 5 requests/hour; legacy: legacy mode, no setting also means legacy mode.")
	flag.BoolVar(&modularMode, "M", false, "modular mode for random delay time")
	flag.IntVar(&afterMilliseconds, "D", 0, "delay time, aka. request url after milliseconds, >= 0")
	flag.IntVar(&numOfTimes, "n", 1, "legacy mode: Request n times, >= 1; others: n seconds/minutes/hours")
	flag.StringVar(&bearer, "B", "", "Authorization Header, bearer format")
	//flag.Usage = usage
}

func usage() {
	fmt.Printf("Usage: %s [options] url\n", os.Args[0])
	flag.PrintDefaults()
}

func main() {
	flag.Parse()

	fmt.Println("*****")
	fmt.Println(startTime)
	fmt.Println("runMode", runMode)
	fmt.Println("numOfTimes", numOfTimes)
	fmt.Println("numOfParallelRoutines", numOfParallelRoutines)
	fmt.Println("modularMode", modularMode)
	fmt.Println("afterMilliseconds", afterMilliseconds)
	fmt.Println("*****")

	if flag.NArg() != 1 || numOfTimes <= 0 || numOfParallelRoutines <= 0 || afterMilliseconds < 0 {
		usage()
		os.Exit(99)
	}
	urls = flag.Args()

	//fmt.Println("ooxx")
	//fmt.Println(flag.NArg(), urls)

	runMode = strings.TrimSpace(strings.ToLower(runMode))

	if runMode == "" || runMode == "legacy" {
		tokenChannel := make(chan int, numOfParallelRoutines)
		for i := 0; i < numOfTimes; i++ {
			if i >= numOfParallelRoutines {
				finishedToken := <-tokenChannel
				fmt.Println("Done token:", finishedToken, time.Now().Sub(startTime))
			}

			id := i
			go doRequest(id, tokenChannel)
		}

		for i := 0; i < numOfParallelRoutines; i++ {
			finishedToken := <-tokenChannel
			fmt.Println("Done token:", finishedToken, time.Now().Sub(startTime))
		}
	} else {
		reg, _ := regexp.Compile("^(\\d+)rp([smh])$")
		match := reg.FindStringSubmatch(runMode)
		if len(match) != 3 {
			fmt.Printf("Unkown run mode: %s, %v\n", runMode, match)
			os.Exit(91)
		}

		numOfReq, _ := strconv.Atoi(match[1])

		timeUnit := (map[string]int{
			"s": 1 * 1000 * 1000,
			"m": 60 * 1000 * 1000,
			"h": 60 * 60 * 1000 * 1000,
		})[match[2]]

		fmt.Printf("%d ... %d\n", numOfReq, timeUnit)

		results := make(chan RequestResult, numOfReq*2)

		for i := 0; i < numOfTimes; i++ {

			sT := time.Now()
			fmt.Printf("%02d.%s\n", 0, sT.Format(TimeFormat))

			for c := 0; c < numOfReq; c++ {
				dT := rand.Int() % timeUnit

				go func() {
					time.Sleep(time.Duration(dT) * time.Microsecond)
					startedTime := time.Now()
					data, err := request()
					endedTIme := time.Now()
					results <- RequestResult{
						startedTime: startedTime,
						endedTIme:   endedTIme,
						data:        data,
						err:         err,
					}
				}()

			}

			for c := 0; c < numOfReq; c++ {
				result := <-results
				success := "success"
				if result.err != nil {
					success = "fail"
				}
				fmt.Printf("%02d.%s %s\t%s\n", c, result.startedTime.Format(TimeFormat), success, result.endedTIme.Sub(result.startedTime))
				if result.err != nil {
					fmt.Println(result.err)
				} else {
					fmt.Println(string(result.data))
				}
			}

			time.Sleep(time.Duration(timeUnit)*time.Microsecond - time.Now().Sub(sT))
		}

	}
}

func doRequest(id int, tokenChannel chan int) {
	defer func() { tokenChannel <- id }()
	// waiting
	var delay = 0
	if afterMilliseconds == 0 {
		delay = 0
	} else if modularMode {
		delay = rand.Int() % afterMilliseconds
	} else {
		delay = afterMilliseconds
	}
	time.Sleep(time.Duration(delay) * time.Millisecond)

	sT := time.Now()

	// request
	data, err := request()
	if err != nil {
		log.Fatalln(err)
		return
	}
	fmt.Println(id, time.Now().Sub(sT), "result:", string(data))
}

func request() ([]byte, error) {
	// request
	req, err := http.NewRequest("GET", urls[0], nil)
	if err != nil {
		//log.Println(err)
		return nil, err
	}

	req.Header.Add("Accept", `application/json`)
	if len(bearer) != 0 {
		req.Header.Add("Authorization", "Bearer "+bearer)
	}
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}
	//client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		//log.Println(err)
		return nil, err
	}
	defer resp.Body.Close()
	return ioutil.ReadAll(resp.Body)
	/*if err != nil {
		log.Fatal(err)
		return
	}*/
}
